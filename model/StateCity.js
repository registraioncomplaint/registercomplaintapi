const mongoose = require('mongoose');
const Schema = mongoose.Schema;



let StateCitySchema = new Schema({
    City : {type:String},
	State : {type:String},
	District : {type:String},
        
});

module.exports = mongoose.model('StateCity', StateCitySchema);