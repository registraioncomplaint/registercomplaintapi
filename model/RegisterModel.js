const mongoose = require('mongoose');
const Schema = mongoose.Schema;



let RegisterSchema = new Schema({
    FirstName: {type: String, max: 100},
    LastName: {type: String,  max: 100},
    Age: {type: Number},
    Email: {type: String, max: 100, unique:false},
    MobileNumber: {type: Number},
    Gender:{type:String,  max:100},
    City:{type:String, max:100},
    State:{type:String, max:100},
    Country:{type:String, max:100},
    ComplaintType:{type:String},
    Scheme:{type:String , default:''},
    ComplaintStatus :{
        type:String,
        default:'Open' 
        },
     CreatedAt: {
            type: Date,
            default: Date
          },
     ComplaintComments:{type:String},
     ComplaintFileName:{type:String},
     ComplaintFile:{type:String},
     resolutionremarks:{type:String, default :'No Remarks'},
     ComplaintID : {type:String , 
     
      unique:true ,
     default: new Date().toString().replace(":","").replace(":","").substring(23,15)}

        
});

module.exports = mongoose.model('RegisterComplaints', RegisterSchema);