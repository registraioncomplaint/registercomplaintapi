const express = require('express');
const router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
const product_controller = require('../controllers/product.controller');


// a simple test url to check that all of our files are communicating correctly.
router.get('/test', product_controller.test);
router.get('/create', product_controller.Register_create);
router.get('/MAPP',product_controller.MAPP_Design);
module.exports = router;