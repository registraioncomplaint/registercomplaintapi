const express = require('express');
var bodyParser = require('body-parser');
var config = require('./config.js/config');
const mongoose = require('mongoose');
var nodemailer = require('nodemailer');

const MongoClient = require('mongodb').MongoClient;

const app = express();
const port = config.port;
const url = config.dbURL;
const mongoDB = process.env.MONGODB_URI || url;

const MAPPs = require('./model/MobileDesignModel'); // importing mobile title schema to get title.
const Register= require('./model/RegisterModel');  // importing Register schema to do CRUD for register schema
const StateCityval= require('./model/StateCity');
const AdminModel = require('./model/Admin');
const SchemeModel = require('./model/Scheme');
const AppRoutes = require('./routes/AppRoutes');  // currently not using these routs

var cors = require('cors')

// This is for allowing cors 


// This is for setting up max limit because request is reciving large obj with base64 of image
//app.use(bodyParser.json({limit: '50000000mb'}));

app
  .use(cors())
  .use(bodyParser.urlencoded({ extended: true }))
  .use(bodyParser.json())

app.use(bodyParser.urlencoded({
  extended: true
}));

bodyParser = require('body-parser').json();

app.listen(port, () => {
  console.log('Server is up and running on port number ' + port); // just to make sure server is running or not

  functionToCheckConnectionAndReturnConnecitonObj();

});


// Function to check the connection to mongodb and logs error if error occurs or else it will return db object.
functionToCheckConnectionAndReturnConnecitonObj =()=>
{
  mongoose.connect(mongoDB);
  mongoose.Promise = global.Promise;
  const db = mongoose.connection;
  db.on('error', console.error.bind(console, 'MongoDB connection error:'));

  return db;
  
}

// This route to get the details of mobile app title and footer title 

app.get('/GetTitle', bodyParser,function(req,res){

var connection = functionToCheckConnectionAndReturnConnecitonObj();

connection.on('error', console.error.bind(console, 'connection error:'));
connection.once('open', function () {

  connection.db.collection("MAPP", function(err, collection){
      collection.find({}).toArray(function(err, data){
          console.log(data); // it will print your collection data
          res.send(data)
      })
  });

});
})

// This variable to create unique id on basis of timestamp.
let ComplaintIDtoSend = '';

app.post('/RegisterNewComplaint', bodyParser, function(req,res){


   ComplaintIDtoSend = new Date().toString().replace(":","").replace(":","").substring(23,15);


    
   let RegisterColleciton = new Register(
        {
            FirstName: req.body.FirstName,
            LastName: req.body.LastName,
            Age: req.body.Age,
            Email:req.body.Email,
            MobileNumber: req.body.MobileNumber,
            Gender:req.body.Gender,
            City:req.body.City,
            State:req.body.State,
            Country:req.body.Country,
            ComplaintType:req.body.ComplaintType,
            ComplaintComments:req.body.ComplaintComments,
            ComplaintFile:req.body.ComplaintFile,
            ComplaintID:ComplaintIDtoSend,
            Scheme:req.body.scheme,
            ComplaintFileName:req.body.fileName

        }
    );

    RegisterColleciton.save(function (err, result) {
        if (err) {
            console.log("error is"+err);
        }
        if(result)
        {
          res.send(result);
          console.log(result)
          // when the data saved successfully to send email and sms to recipient that complaint has been registered
          sendEmailToRegistered(req.body.FirstName ,req.body.Email , ComplaintIDtoSend , req.body.ComplaintType);
          sendSMSToRegisterd(req.body.FirstName ,req.body.MobileNumber , ComplaintIDtoSend , req.body.ComplaintType)
        }

       
    })
})

// This function is to build message and return the message which will be used in sendEmail and sendSMS functions.
buildUserMessage =(username, ComplaintIDtoSend)=>
  {
    return 'Hi ' + username +', your complaint ID is ' +ComplaintIDtoSend;
  }

sendEmailToRegistered =(username, Email, ComplaintIDtoSend,ComplaintType)=>
  {
    // used node mailer lib to trigger email
    var transporter = nodemailer.createTransport({
      host: "smtp-mail.outlook.com", // hostname
      secureConnection: false, // TLS requires secureConnection to be false
      port: 587, // port for secure SMTP
      tls: {
      ciphers:'SSLv3'
      },
      requireTLS:true,//this parameter solved problem for me
      auth: {
        user: config.emailUserID,  // this will need to be setup in live
        pass: config.emailPass
      },
      tls: {
          ciphers:'SSLv3'
      } 
    });

    var mailOptions = {
      from: config.emailUserID,
      to: Email,
      subject: 'Complaint Registered',
      text: buildUserMessage(username,ComplaintIDtoSend)
    };

    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log("email error"+error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });
}

  sendSMSToRegisterd=(username='', MobileNumber='', ComplaintIDtoSend='',ComplaintType='')=>
  {

    // Used twillo api service to trigger sms (trail version not paid)
      const accountSid = config.twilloAccountSID;
      const authToken = config.twilloAuthToken
      const client = require('twilio')(accountSid, authToken);

      client.messages
        .create({
          body: buildUserMessage(username,ComplaintIDtoSend),
          from: '+15005550006',
          to: '+91'+MobileNumber
        },(err,responsedata)=>{
          if(err)
          {
            console.log("twillo error"+err)
          }
          if(responsedata)
          {
         //   console.log(responsedata)
          }
        })
        .then(message => console.log(message.sid));


        const Nexmo = require('nexmo');

const nexmo = new Nexmo({
  apiKey: '288e3904',
  apiSecret: 'UDbOn2jEuLk5qwC7',
});

const from = '919738264660';
const to = '91'+MobileNumber;
const text = buildUserMessage(username,ComplaintIDtoSend)


nexmo.message.sendSms(from, to, text);


nexmo.message.sendSms(from, to, text, (err, responseData) => {
    if (err) {
        console.log(err);
    } else {
        if(responseData.messages[0]['status'] === "0") {
            console.log(responseData);
        } else {
            console.log(`Message failed with error: ${responseData.messages[0]['error-text']}`);
        }
    }
})
  }


 // This function is just to make sure api end point is working fine or not
  app.post('/',bodyParser, function(req,res){

       res.send(req.body.sayhello)

  })

// This route is to get details of country listing.
  app.get('/GetCountry', bodyParser ,function(req,res){

      var connection = functionToCheckConnectionAndReturnConnecitonObj();
      
      connection.on('error', console.error.bind(console, 'connection error:'));
      connection.once('open', function () {
      
        connection.db.collection("Country", function(err, collection){
            collection.find({}).toArray(function(err, data){
                console.log(JSON.stringify(data));
                console.log(data)
                res.send(JSON.stringify(data))
            })
        });
      
      });
  })

// This route is to get the details of  states
  app.post('/GetState', bodyParser, function(req,res){

      var connection = functionToCheckConnectionAndReturnConnecitonObj();
      
      connection.on('error', console.error.bind(console, 'connection error:'));
      connection.once('open', function () {
      
        connection.db.collection("State", function(err, collection){
            collection.find({CountryCode:req.body.CountryCode}).toArray(function(err, data){
                console.log(JSON.stringify(data));
                console.log(data)
                res.send(JSON.stringify(data))
            })
        });
      
      });
  })

// This api route is to get the list of cites which belongs to selected state
  app.post('/GetStateCity', bodyParser, function(req,res){

      var connection = functionToCheckConnectionAndReturnConnecitonObj();
      
      connection.on('error', console.error.bind(console, 'connection error:'));
      connection.once('open', function () {
      
        connection.db.collection("City", function(err, collection){
            collection.find({StateCode:req.body.StateCode}).toArray(function(err, data){
                console.log(JSON.stringify(data));
                console.log(data)
                res.send(JSON.stringify(data))
            })
        });
      
  });
  })

  // This route to provide the complaint details based on user passed request
  app.post('/GetComplaints', bodyParser,(req,res)=>{
   

    console.log(req.body.MobileNumber);
    const Register= require('./model/RegisterModel');
// Search by mobile number and return res
    if((req.body.ComplaintID === '' ||req.body.ComplaintID === undefined) && (req.body.Email ==='' || req.body.Email === undefined) && (req.body.MobileNumber !== '' || req.body.MobileNumber !== undefined))
    {

      Register.find({ 'MobileNumber': req.body.MobileNumber}, 'State ComplaintID FirstName LastName Email MobileNumber Age City State Scheme ComplaintStatus ComplaintType ComplaintComments CreatedAt ComplaintFileName', function (err, data) {
        if(err)
        {
res.send(err);
        }
        if(data)
        {
          console.log(data);
          res.send(data)
        }
     
      })
    }
// Search by email id and return res
    if((req.body.ComplaintID === '' ||req.body.ComplaintID === undefined) && (req.body.MobileNumber ==='' || req.body.MobileNumber === undefined))
    {

      Register.find({ 'Email': req.body.Email} , 'State ComplaintID FirstName LastName Email MobileNumber Age City State Scheme ComplaintStatus ComplaintType ComplaintComments ComplaintFileName', function (err, data) {
        if (err) return handleError(err);
       console.log(data);
       res.send(data)
      })
    }
// Search by Complaint id and return res
    if((req.body.Email === '' ||req.body.Email === undefined) && (req.body.MobileNumber ==='' || req.body.MobileNumber === undefined))
    {

      Register.find({ 'ComplaintID': req.body.ComplaintID} , 'State ComplaintID Scheme FirstName LastName Email MobileNumber Age City State ComplaintStatus ComplaintType ComplaintComments CreatedAt ComplaintFileName', function (err, data) {
        if (err) return handleError(err);
       console.log(data);
       res.send(data)
      })
    }

  })

// This router to display the list of complaint wrt provided date range.
  app.post('/GetListbyDate',bodyParser,(req,res)=>{

      let startDate = req.body.StartDate;

      let endDate = req.body.EndDate;

      let status = req.body.Status;

      console.log(startDate);
      console.log(endDate);


      const Register= require('./model/RegisterModel');

      if(status == "All")
      {
        Register.find({

          "CreatedAt" : {"$gte": startDate,
                      "$lt": endDate}
  
  
        }, 'State ComplaintID Age City State FirstName LastName MobileNumber Email Scheme ComplaintStatus ComplaintType ComplaintComments CreatedAt ComplaintFileName _id resolutionremarks', function(err,result){
          if(err)
          {
            console.log(err)
          }
          if(result)
  
          {
            console.log(result);
            res.send(result)
          }
  
        })
      }
      else
      {
        Register.find({

          "CreatedAt" : {"$gte": startDate,
                      "$lt": endDate},
  
          
          "ComplaintStatus": req.body.Status
  
        }, 'State ComplaintID Scheme ComplaintStatus Age City State FirstName LastName MobileNumber Email ComplaintType ComplaintComments CreatedAt ComplaintFileName _id resolutionremarks', function(err,result){
          if(err)
          {
            console.log(err)
          }
          if(result)
  
          {
            console.log(result);
            res.send(result)
          }
  
        })
      }
      
     
      
  })

 

  app.get('/GetStateNames', bodyParser,(req,res)=>{

    
    const statecity= require('./model/StateCity');

    statecity.find().distinct('State', function(err, result){
      if(err)
      {
        res.send(err);
      }
      else
      {
        res.send(result);
      }
    })

  });


  app.post('/GetCityNames',bodyParser, (req,res)=>{
    
     let stateName = req.body.State;

     const statecity= require('./model/StateCity');

     statecity.find({State:stateName},function(err, result){
       if(err)
       {
         res.send(err);
       }
       else
       {
         res.send(result);
       }
     })
    });

    // This is to check the user is admin or not.
    

  

// This is to close the complaint based on complaint object id 
    app.post('/ComplaintFindUpdate',bodyParser,(req,res)=>{

      let id = req.body.id;
      let resolutionremarksfrombody = req.body.remarks;
      let updatedStatus ='Closed';
      const Register= require('./model/RegisterModel');

      Register.findByIdAndUpdate({_id:id}, {"ComplaintStatus": updatedStatus , "resolutionremarks": resolutionremarksfrombody}, function(err,result){
        if(err)
        {
          console.log(err);
        }
        if(result)
        {
          console.log(result);
          res.send("Complaint has been closed")
        }
      })

    })


    app.post('/ComplaintFindUpdateToProgress',bodyParser,(req,res)=>{

      let id = req.body.id;
      let updatedStatus ='In Progress';
      const Register= require('./model/RegisterModel');

      Register.findByIdAndUpdate({_id:id}, {"ComplaintStatus": updatedStatus}, function(err,result){
        if(err)
        {
          console.log(err);
        }
        if(result)
        {
          console.log(result);
          res.send("Complaint Status has been changed to In Progress")
        }
      })

    })

    app.post('/sendFilie', bodyParser,(req,res)=>{
      let id = req.body.id;
    
      const Register= require('./model/RegisterModel');

      Register.findById({_id:id},'ComplaintFileName', function(err,result){
        if(err)
        {
          console.log(err);
        }
        if(result)
        {
          console.log(result);
          res.send(result);
        }
      })

    })


    app.get('/addstate', (req,res)=>{

    
      let StateCity = new StateCityval(
        {
          City : req.body.city,
          State : req.body.state,
          District :req.body.district,

        }
    );

    StateCity.save(function (err, result) {
        if (err) {
            console.log("error is"+err);
        }
        if(result)
        {
          res.send(result);
          console.log(result)
        }

       
    })
    })

    app.get('/addAdmin', (req,res)=>{

    
      let AdminMod = new AdminModel(
        {
          AdminName : req.body.adminName,
          AdminPass : req.body.adminPass

        }
    );

    AdminMod.save(function (err, result) {
        if (err) {
            console.log("error is"+err);
        }
        if(result)
        {
          res.send(result);
          console.log(result)
        }

       
    })
    })


    app.post('/CheckAdmin',bodyParser,(req,res)=>{

      let AdminPassword = req.body.AdminPassword;
      let AdminOrNOt = 'isNotAdmin';
      var connection = functionToCheckConnectionAndReturnConnecitonObj();

  
      connection.on('error', console.error.bind(console, 'connection error:'));
      connection.once('open', function () {
      
        connection.db.collection("AdminList", function(err, collection){
            collection.find({AdminPass:AdminPassword}).toArray(function(err, data){
                console.log(JSON.stringify(data));
                if(err)
                {
                  res.send(AdminOrNOt)
                }
                if(data.length === 0)
                {
                  AdminOrNOt = 'isNotAdmin'
                  console.log(data.length)
                  res.send(AdminOrNOt)
                }
                if(data.length >0)
                {
                  AdminOrNOt = 'isAdmin'
                  console.log(data.length)
                  res.send(AdminOrNOt)
                }
              
            })
        });
      
      });

    })


    app.get('/CheckAdminorNot', bodyParser,(req,res)=>{

    

     adminmodel.find({AdminPass:AdminPassword}, (err,result)=>{
       if(err)
       {
         res.send(AdminPassword);
       }
       
        if(result.length === 0)
        {
          AdminOrNOt = 'isNotAdmin'
          console.log(data.length)
          res.send(AdminPassword)
        }
        if(result.length >0)
        {
          AdminOrNOt = 'isAdmin'
          console.log(data.length)
          res.send(AdminPassword)
        }
       
     })
  
    });


    app.post('/GetAdminDetails',bodyParser, (req,res)=>{
    
      const adminmodel= require('./model/Admin');
      let AdminPassAdminPass = req.body.AdminPassword;
      let AdminOrNOt = 'isNotAdmin';
  
 
      adminmodel.find({AdminPass:AdminPassAdminPass},function(err, result){
        if(err)
        {
          res.send(err);
        }
        else
        {
          if(result.length>0)
          {
            res.send("IsAdmin");
          }
          else
          {
            res.send("NotAdmin")
          }
        }
      })
     });
  

     app.post('/AddingNewScheme',(req,res)=>{
       let SchemeName = req.body.schemeName;
       let SchemeOwneremail = req.body.schemeownemail;
       let SchemeTemplate = req.body.schemeTemplate;


       
      let schemeModel = new SchemeModel(
        {
          SchemeName: SchemeName,
          SchemeOwner:SchemeOwneremail,
          SchemeTemplet: SchemeTemplate,

        }
    );

    schemeModel.save(function (err, result) {
        if (err) {
            console.log("error is"+err);
            res.send("Scheme name already exists, Please provide different scheme name")
        }
        if(result)
        {
          res.send("Saved Successfully");
          console.log("Saved Successfully")
        }

       
    })

       
     })

     app.get('/getSchemeDetails',(req,res)=>{

      let schemeSchema = require('./model/Scheme');

      schemeSchema.find({},function(err,result){
        if(err)
        {
          console.log("Some error has been occured"+err);
        }
        if(result)
        {
          console.log(result);
          res.send(result);
        }
      })

     })

     app.post('/deleteScheme',(req,res)=>{

      let objId = req.body.objId;
      let schemeSchema = require('./model/Scheme');

      schemeSchema.remove({_id:objId}, function(err,result){
        if(err)
        {
          console.log("some error "+err);
          res.send("error")
        }
        else
        {
          console.log(result);
          res.send("deleted sucessfully");
        }
      })



     })

     app.post('/findAndUpdateScheme',(req,res)=>{


      let objId = req.body.objId;
      let schemeName = req.body.schemeName;
      let schemeOwner = req.body.schemeOwnEmail;
      let schemeTemplate = req.body.schemeTemplate;

      let schemeSchema = require('./model/Scheme');

      schemeSchema.findByIdAndUpdate({_id:objId}, {"SchemeName": schemeName , "SchemeOwner": schemeOwner , "SchemeTemplet":schemeTemplate}, function(err,result){


         if(err)
         {
           console.log("some error "+err);
           res.send("Some error has been occurred while updating please try again")
         }
         else
         {
           console.log(result);
           res.send("Updated Successfully");
         }

     })

    })
  

